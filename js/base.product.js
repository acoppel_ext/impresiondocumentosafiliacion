/**************************************************************************************************************************************
 * <VARIABLES>
**************************************************************************************************************************************/
//#region
//GLOBALES
var cTituloModal 		= "Firma de documentos";
var cMensaje 			= "";
var ligaCase 			= "php/casebase.php";
var iEmpleado 			= 0;
var iFolioSol 			= 0;
var rutaDoc 			= "../visorpdf/indexvisorpdf.html?";
var rutaUFAF 			= "";
var rutaRelativa 		= "";
var MensajePar			= "";
var nombrePdfUFAF 		= "";
var enrol 				= 0;//Tiene enrolamiento (0-No/1-Si)
var iBusqueda 			= 2;//Pedir busqueda de huellas (2-enrolamiento/4-captura de indices de la afiliaicon)
var iRenapo 			= 0;
var arrSO 				= new Array();
var arrCatCorreo 		= new Array();
var arrDatosEnvio 		= new Array();
var arrIps 				= new Array();
var arrDatosAfi			= new Array();
var arrImpresiones		= new Array();
var arrRespHuellaSello 	= [
	{solicitante: 2, error: 100, mensaje: "PROMOTOR: La huella no coincide con la de tu enrolamiento. Favor de intentar de nuevo."},
	{solicitante: 1, error: 101, mensaje: "PROMOTOR: Se presentó un problema al capturar la huella del trabajador. Favor de intentar de nuevo."},
	{solicitante: 2, error: 101, mensaje: "PROMOTOR: Se presentó un problema al capturar tu huella. Favor de intentar de nuevo."},
	{solicitante: 1, error: 102, mensaje: "PROMOTOR: Informarle al trabajador que la huella del índice derecho es igual al del promotor. Favor de intentar de nuevo."},
	{solicitante: 2, error: 102, mensaje: "PROMOTOR: Tu huella del &iacutendice derecho es igual al del Trabajador. Favor de intentar de nuevo."},
	{solicitante: 1, error: 103, mensaje: "PROMOTOR: El trabajador tiene un problema con la huella capturada te sugiero que cambie de huella. Favor de intentar de nuevo."},
	{solicitante: 2, error: 103, mensaje: "PROMOTOR: Tienes un problema con la huella capturada te sugiero cambiar de huella. Favor de intentar de nuevo."},
	{solicitante: 1, error: 104, mensaje: "PROMOTOR: Informarle al trabajador que ocurrió un error al intentar llamar la aplicación. Favor de intentar de nuevo."},
	{solicitante: 2, error: 104, mensaje: "PROMOTOR: Ocurrió un error al intentar llamar la aplicación. Favor de intentar de nuevo."},
	{solicitante: 1, error: 105, mensaje: "PROMOTOR: El trabajador no tiene un enrolamiento aceptado y no debes realizar solicitudes. Favor de consulta el estatus de tu enrolamiento."},
	{solicitante: 2, error: 105, mensaje: "PROMOTOR: No tienes un enrolamiento aceptado y no debes realizar solicitudes. Consulta el estatus de tu enrolamiento."},
	{solicitante: 1, error: 106, mensaje: "PROMOTOR: Comunícale al trabajador que las huellas capturadas no coinciden con ninguna de su enrolamiento. El trámite se deberá iniciar nuevamente."},
	{solicitante: 2, error: 106, mensaje: "PROMOTOR: Las huellas capturadas no coinciden con ninguna de tu enrolamiento. El trámite se deberá iniciar nuevamente."},
	{solicitante: 1, error: 107, mensaje: "Proceso cancelado por el usuario. Favor de intentar de nuevo."},
	{solicitante: 2, error: 107, mensaje: "Proceso cancelado por el usuario. Favor de intentar de nuevo."},
];
var selloverificacionbiom = '';
var diagnosticovoluntram = '';
var resultadoverificacion = '';
var mensaje2do = '';
var contador2do = 1;
var error2do = 'Error';

var selloverificacionbiom = '';
var diagnosticovoluntram = '';
var resultadoverificacion = '';
var mensaje2do = '';
var contador2do = 1;
var error2do = 'Error';

//SERVICIO WEBX
var iOpcion 			= 0;
var iContHuellas 		= 0;
var iHuella 			= 0;
var tipodigitalizacion 	= 0;
var HUELLA_TRABAJADOR 	= 1;
var HUELLA_PROMOTOR 	= 2;
var FIRMA_TRABAJADOR 	= 3;
var FIRMA_PROMOTOR 		= 4;
var DIGITALIZADOR 		= 5;

var OSName = "";
//#endregion

/**************************************************************************************************************************************
 * <INICIO>
**************************************************************************************************************************************/
//#region
$(document).ready(function () {
	//Obtener el SO
	var divRespuestaRenapo 	= document.getElementById('divRespuestaRenapo');
	var addRespuesta = '';
	arrSO = sistemaOperativo();
	grabarRegistro("ready - arrSO ---->" + arrSO.os);

	if (navigator.appVersion.indexOf("Win") != -1) OSName = "Windows";
	if (navigator.appVersion.indexOf("Mac") != -1) OSName = "MacOS";
	if (navigator.appVersion.indexOf("X11") != -1) OSName = "UNIX";
	if (navigator.appVersion.indexOf("Linux") != -1) OSName = "Linux";
	if (navigator.appVersion.indexOf("Android") != -1) OSName = "Android";

	$.ajaxSetup({
		async: false,
        cache: false,
		type: "POST",
		dataType: "json",
		error : function(a, b, c) {
			switch (a.status) {
				case 404:
                    grabarRegistro("[404]: Pagina no Encontrada");
                    break;
                case 401:
                    mostrarMensajeCierreSesion();
                    break;
                default:
                    grabarRegistro("[default]:" + a + b + c);
                    break;
			}
		}
    });

	//Obtener Variables de URL
	mdlEspere(cTituloModal);
	iEmpleado = obtenerVarialbesURL("empleado");
	iFolioSol = obtenerVarialbesURL("folio");
	iRenapo = obtenerVarialbesURL("renapo");
	//Limpiar URL
	cortarURL();
	if (iEmpleado > 0 && iFolioSol > 0) {
		grabarRegistro("Inicia pagina de impresion");
		datosPagina();
		obtenerEmpleado();
		obtenerCatalogoCorreo();
		obtenerDatosSolicitudAfi();
		if(iRenapo == 1)
		{
			divRespuestaRenapo.innerHTML = '';
			addRespuesta += '<input style="cursor:default" autocomplete="off" type = "image" src="imagenes/RenapoExitoso.png" width = "132" height = "28"></input>';
			divRespuestaRenapo.innerHTML = addRespuesta;
		}	
	} else {
		cMensaje = "Error - parametros, favor de contactar a mesa de ayuda.";
		mdlMsjFunc(cTituloModal,cMensaje,"ACEPTAR","cierraModal()");
	}

	$("#btnFirmaTrab").click(function(){
		iHuella = 1;
		capturaFirmaUnificada(1);
	});

	$("#btnFirmaProm").click(function(){
		iHuella = 1;
		capturaFirmaUnificada(2);
	});

	$("#btnExpediente").click(function(){
		if(OSName == "Android"){
			capturaCorreo();
		}else{
			medioEntrega();
		}
	});

	$(document.body).on("change","#slcEntrega",function (e) {
		var optVal = $("#slcEntrega option:selected").val();
		if(optVal == 1){
			cMensaje = "PROMOTOR: Debes colocar al inicio la hoja tamaño oficio para imprimir la Solicitud de Afiliación";
			mdlMsjFunc(cTituloModal,cMensaje,"Imprimir","imprimirDocumnetos();");
		}else if(optVal == 2){
			capturaCorreo();
		}
	});

	$(document.body).on("change","#slcCorreo01",function (e) {
		var optVal = $("#slcCorreo01 option:selected").val();
		if(optVal == 7){
			$(".confirmacion01").removeClass("none");
			$("#ipDominio01").val("");
		}else{
			$(".confirmacion01").addClass("none");
		}
	});

	$(document.body).on("change","#slcCorreo02",function (e) {
		var optVal = $("#slcCorreo02 option:selected").val();
		if(optVal == 7){
			$(".confirmacion02").removeClass("none");
			$("#ipDominio02").val("");
		}else{
			$(".confirmacion02").addClass("none");
		}
	});

	$("#btnDigitaliza").click(function(){
		$(".btn-coppel").attr("disabled",true);
		opcionWebService(DIGITALIZADOR);
	});
});
//#endregion

/**************************************************************************************************************************************
 * <FUNCIONES GENERALES>
**************************************************************************************************************************************/
//#region
function grabarRegistro(cTexto) {
	$.post(ligaCase, { opcion: 1, arrdatos: cTexto}
	).done(function (result) {
		console.log(result);
	});
}

function datosPagina() {

	$.post(ligaCase, { opcion: 2,}
	).done(function (result) {
		$("#fechaActual").text(result.fechaactual);
		arrIps = {ipspa: result.ipspa, ipmodulo: result.ipmodulo};
		cMensaje = "IPSPA [" + arrIps.ipspa + "] IPMODULO [" + arrIps.ipmodulo + "]";
		grabarRegistro(cMensaje);
	});
}
//#endregion

/**************************************************************************************************************************************
 * <FUNCIONES VALIDACIONES>
**************************************************************************************************************************************/
//#region
function obtenerEmpleado() {

	arrDatosEnvio = { iEmpleado: iEmpleado };
	$.post(ligaCase, { opcion: 3, funcion: "obtnerinformacionpromotor", arrdatos: arrDatosEnvio }
	).done(function (result) {
		if (result.estatus == 1) {
			if (result.registros.icodigo == 0) {
				$("#nombrePromo").text(correcionCaracteres(result.registros.cnombre));
				$("#tiendaPromo").text(result.registros.ctienda);
			} else {
				cMensaje = result.registros.cmensaje;
				grabarRegistro(cMensaje);
				mdlMsjFunc(cTituloModal,cMensaje,"ACEPTAR","cierraModal()");
			}
		} else {
			cMensaje = "Error - API información empleado -> " + result.descripcion;
			grabarRegistro(cMensaje);
			mdlMsjFunc(cTituloModal,cMensaje,"ACEPTAR","cierraModal()");
		}
	});
}

function obtenerCatalogoCorreo(){
	$.post(ligaCase, { opcion: 3, funcion: "consultarcatalogocorreoei"}
	).done(function (result) {
		if (result.estatus == 1) {
			arrCatCorreo = result.registros;
		} else {
			cMensaje = "Error - API Catalogo correos -> " + result.descripcion;
			grabarRegistro(cMensaje);
			mdlMsjFunc(cTituloModal,cMensaje,"ACEPTAR","cierraModal()");
		}
	});
}

function obtenerDatosSolicitudAfi() {
	arrDatosEnvio = { iFolioSol: iFolioSol };
	$.post(ligaCase, { opcion: 3, funcion: "obtenerdatossolicitudafi", arrdatos: arrDatosEnvio }
	).done(function (result) {
		if (result.estatus == 1) {
			if (result.registros.respuesta == 1) {
				arrDatosAfi = result.registros;
				/*respuesta,tiposolicitante,autenticacion,tiposolicitud,tipotraspaso,curp,nss,nombre,paterno,materno,folioenrol,manosenrol,enrolpermanente,email*/
				if (arrDatosAfi.tiposolicitud == 27){tipodigitalizacion = 27;}
				else {tipodigitalizacion = 26;}
				obtencionSello();
			} else {
				cMensaje = "Promotor, no se encontro registro del trabajdor, favor de contactar a mesa de ayuda!";
				grabarRegistro(cMensaje);
				mdlMsjFunc(cTituloModal,cMensaje,"ACEPTAR","cierraModal()");
			}
		} else {
			cMensaje = "Error - API Datos Folio -> " + result.descripcion;
			grabarRegistro(cMensaje);
			mdlMsjFunc(cTituloModal,cMensaje,"ACEPTAR","cierraModal()");
		}
	});
}

async function validarregistroenrolhuellaservicio() {
	var bResp 		= false;
	var iTipoOpc 	= 0;
	var arrResp 	= new Array();
	if(iOpcion == HUELLA_TRABAJADOR){iTipoOpc = 3;}
	else if (iOpcion == HUELLA_PROMOTOR){iTipoOpc = 2;}
	arrDatosEnvio = { iFolioSol: iFolioSol, iTipoSol: iTipoOpc };
	arrResp = await $.post(ligaCase,{ opcion: 3, funcion: "validarregistroenrolhuellaservicio", arrdatos: arrDatosEnvio });
	if (arrResp.estatus == 1) {
		if (arrResp.registros.respuesta == 1) {
			bResp = true;
		}
	} else {
		cMensaje = "Error - API Validar registro huellas sello -> " + result.descripcion;
		grabarRegistro(cMensaje);
		mdlMsjFunc(cTituloModal,cMensaje,"ACEPTAR","cierraModal()");
	}

	return bResp;
}
//#endregion

/**************************************************************************************************************************************
 * <FUNCIONES UNIFICACION DE FIRMAS>
**************************************************************************************************************************************/
//#region
function obtencionSello() {

	/***
	 * "SELECT * FROM cattiposolicitante;" => tiposolicitante:
	 * 1-TITULAR|2-BENEFICIARIO|3-APODERADO/REPRESENTANTE LEGAL|4-CURADOR|5-TUTOR
	***/
	if (arrDatosAfi.tiposolicitante == 1){
		//Validar si tiene enrolamiento capturado y como debe pedir el 2do sello (enrolpermanente => 0-AFORE/1-PROSESAR)
		if(arrDatosAfi.enrolpermanente == 1){
			iBusqueda = 4;
		}else{
			iBusqueda = 2;
			enrol = 1;
		}
		//Validar si tiene excepciones de huellas para solo pedir la huella del promotor
		if(arrDatosAfi.manosenrol == 1){
			capturaHuellaSegundoSello(2);
		}else{
			capturaHuellaSegundoSello(1);
		}
	}else{
		//mdlMsjFunc(cTituloModal,"Firma de Trabajador.","ACEPTAR","capturaFirmaUnificada(1);");
		capturaFirmaUnificada(1);
	}
}

function capturaHuellaSegundoSello(opc) {
	/*** opc (1-Trabajador/2-Promotor) ***/
	cMensaje = "capturaHuellaSegundoSello => " + opc;
	iContHuellas++;
	grabarRegistro(cMensaje);
	if(opc == 1){opcionWebService(HUELLA_TRABAJADOR);}
	else{opcionWebService(HUELLA_PROMOTOR);}
}

async function respuestaHuellaSegundoSello(resp) {
	var bResp 		= false;
	var fFuncion 	= "";
	if(iOpcion == HUELLA_TRABAJADOR){fFuncion = "capturaHuellaSegundoSello(1);";}
	else if (iOpcion == HUELLA_PROMOTOR){fFuncion = "capturaHuellaSegundoSello(2);";}

	if(resp == 1){
		bResp = await validarregistroenrolhuellaservicio();
		if(bResp){
			if(iOpcion == HUELLA_TRABAJADOR){
				iContHuellas = 0;
				capturaHuellaSegundoSello(2);
			}else if (iOpcion == HUELLA_PROMOTOR){
				//alert ("consultaWs2 INTENTO NUMERO:" + contador2do); ///////////////Descomentar para emular 2do sello  
				consultarrespuestawebservices(iFolioSol,iEmpleado);	
				//capturaHuellaSegundoSello(2);
				   				
					setTimeout(function (){
						if(!((selloverificacionbiom == "" || selloverificacionbiom == "0") 
						&& diagnosticovoluntram == "" && mensaje2do == '' && resultadoverificacion == ""))
						{
								//Si la respuesta del webservice fue un exito
							if (resultadoverificacion == "01" || diagnosticovoluntram == "D96" || diagnosticovoluntram == "E13" ||
								diagnosticovoluntram == "E14" || diagnosticovoluntram == "E22" || diagnosticovoluntram == "E23") 
							{
								contador2do = 0;
								//capturaHuellaSegundoSello(2);
								//Respuesta exitosa del webservice, continuar flujo
								capturaFirmaUnificada(1);
								grabarRegistro(" Respuesta exitosa del webservice, continuar flujo. ");
							}
							else //si respuesta del webservices no fue exito
							{
								//Si el codigo de respueta del webservices esta dentro de los posibles reintentos.
								if (contador2do <= 3 && diagnosticovoluntram == "028" || diagnosticovoluntram == "D97" ||
									diagnosticovoluntram == "D99" || diagnosticovoluntram == "E03" || diagnosticovoluntram == "E02" ||
									diagnosticovoluntram == "E11" || diagnosticovoluntram == "E16" || diagnosticovoluntram == "E18" || 
									diagnosticovoluntram == "E19" || diagnosticovoluntram == "E20" || diagnosticovoluntram == "E21" || 
									diagnosticovoluntram == "E26" || diagnosticovoluntram == "E27" || diagnosticovoluntram == "E31" ||
									diagnosticovoluntram == "E36" || diagnosticovoluntram == "E37" || diagnosticovoluntram == "E04" ||
									diagnosticovoluntram == "E70" || diagnosticovoluntram == "E28" || diagnosticovoluntram == "E40" ||
									diagnosticovoluntram == "E42" || diagnosticovoluntram == "E43" || diagnosticovoluntram == "E44" ||
									diagnosticovoluntram == "E45" || diagnosticovoluntram == "E46")
								{									
									if(contador2do == 3)
									{
										var cTituloModal2 = 'Listado de Documentos';
										// mdlMsjCerrar(cTituloModal2,mensaje2do,"ACEPTAR","cierraModal()");
										mdlMsjFunc(cTituloModal2,mensaje2do,"ACEPTAR","cierraModal()");
										grabarRegistro("Se han realizado los 3 intentos. Se procede a la cancelacion. " + diagnosticovoluntram);

										if(mensaje2do == null || mensaje2do == '' || mensaje2do == 'NA')
										{
											var cTituloModal2 = 'Listado de Documentos';
											mensaje2do = 'SE PRESENTÓ UN ERROR EN LA OBTENCIÓN DEL SELLO, FAVOR DE COMUNICARSE A MESA DE AYUDA.';
											//mdlMsjCerrar(cTituloModal2,mensaje2do,"ACEPTAR","cerrarImpresion()");
											mdlMsjFunc(cTituloModal2,mensaje2do,"ACEPTAR","cierraModal()");
										}
									}
									else
									{
										capturaHuellaSegundoSello(1);
										contador2do ++ ;
									}
									grabarRegistro(" Tipo de diagnostico obtenido ---->" + diagnosticovoluntram);
									
								}//Si el codigo de rechazo no permite reintentos.. hay que cancelar de inmediato
								else if (diagnosticovoluntram == "022" || diagnosticovoluntram == "203" || diagnosticovoluntram == "270" || 
										diagnosticovoluntram == "D98" || diagnosticovoluntram == "E05" || diagnosticovoluntram == "E06" ||
										diagnosticovoluntram == "E09" || diagnosticovoluntram == "E10" || diagnosticovoluntram == "E17" ||
										diagnosticovoluntram == "E65" || diagnosticovoluntram == "E01" || diagnosticovoluntram == "049" )
								{
									if(diagnosticovoluntram == "E05" )
									{
										var cTituloModal2 = 'Listado de Documentos';
										mensaje2do = error2do + ' [E05] PROMOTOR: SE PRESENTÓ UN ERROR EN LA OBTENCIÓN DEL SELLO, FAVOR DE COMUNICARSE A MESA DE AYUDA.';
										//mdlMsjCerrar(cTituloModal2,mensaje2do,"ACEPTAR","cierraModal()");
										mdlMsjFunc(cTituloModal2,mensaje2do,"ACEPTAR","cierraModal()");
										grabarRegistro(" Tipo de diagnostico obtenido ---->" + diagnosticovoluntram);
									}
									else if(diagnosticovoluntram == "E06")
									{
										var cTituloModal2 = 'Listado de Documentos';
										mensaje2do = error2do + ' [E06] PROMOTOR: SE PRESENTÓ UN ERROR EN LA OBTENCIÓN DEL SELLO, FAVOR DE COMUNICARSE A MESA DE AYUDA.';
										mdlMsjCerrar(cTituloModal2,mensaje2do,"ACEPTAR","cierraModal()");
										grabarRegistro(" Tipo de diagnostico obtenido ---->" + diagnosticovoluntram);
									}
									else if(mensaje2do == null || mensaje2do == ''|| mensaje2do == 'NA')
									{
										var cTituloModal2 = 'Listado de Documentos';
										mensaje2do = 'SE PRESENTÓ UN ERROR EN LA OBTENCIÓN DEL SELLO, FAVOR DE COMUNICARSE A MESA DE AYUDA.';
										//mdlMsjCerrar(cTituloModal2,mensaje2do,"ACEPTAR","cierraModal()");
										mdlMsjFunc(cTituloModal2,mensaje2do,"ACEPTAR","cierraModal()");
										grabarRegistro(" Tipo de diagnostico obtenido ---->" + diagnosticovoluntram);
									}

									var cTituloModal2 = 'Listado de Documentos';
									//mdlMsjCerrar(cTituloModal2,mensaje2do,"ACEPTAR","cierraModal()");
									mdlMsjFunc(cTituloModal2,mensaje2do,"ACEPTAR","cierraModal()");
									grabarRegistro(" Se tiene un codigo de rechazo inmediato. Se procede a la cancelacion. ");
							}
							else
							{
								contador2do = 0;
								capturaFirmaUnificada(1);
								grabarRegistro(" diagnostico no encontrado en el catalogo.");
							}
						}
					}
					else
					{
						contador2do = 0;
						capturaFirmaUnificada(1);
						grabarRegistro(" Sin respuesta del webservices de Procesar o error en el webservices");
					}
					},3000);
			}
		}else{
			if(iContHuellas > 5){
				if(iOpcion == HUELLA_TRABAJADOR){
					cMensaje = "PROMOTOR: Infórmale al trabajador que excedió el número de intentos. Favor de contactar a Mesa de Ayuda.";
				}else if (iOpcion == HUELLA_PROMOTOR){
					cMensaje = "PROMOTOR: has excedió el número de intentos. Favor de contactar a Mesa de Ayuda.";
				}
				// Si excede el numero de intentos
				mdlMsjFunc(cTituloModal,cMensaje,"ACEPTAR","cierraModal()");
			}else{
				if(iContHuellas == 2){
					cMensaje = "PROMOTOR: La captura de huella no se registró. Favor de volver a intentar.";
				}else{
					if(iOpcion == HUELLA_TRABAJADOR){
						cMensaje = "PROMOTOR: Se presentó un problema al capturar la huella del trabajador. Favor de intentar de nuevo.";
					}else if (iOpcion == HUELLA_PROMOTOR){
						cMensaje = "PROMOTOR: Se presentó un problema al capturar tu huella. Favor de intentar de nuevo.";
					}
				}
				mdlMsjFunc(cTituloModal,cMensaje,"ACEPTAR",fFuncion);
			}
		}
	}else{
		if(iContHuellas > 3){
			if(iOpcion == HUELLA_TRABAJADOR){
				cMensaje = "PROMOTOR: Infórmale al trabajador que excedió el número de intentos.";
			}else if (iOpcion == HUELLA_PROMOTOR){
				cMensaje = "PROMOTOR: Haz excedió el número de intentos.";
			}
			// Si excede el numero de intentos
			mdlMsjFunc(cTituloModal,cMensaje,"ACEPTAR","cierraModal()");
		}else{
			$.each(arrRespHuellaSello, function(key, value){
				if(value.error == resp && value.solicitante == iOpcion){
					bResp 		= true;
					cMensaje 	= value.mensaje;
				}
			});
			if(!bResp){cMensaje = "Error desconocido. Favor de intentar de nuevo.";}
			mdlMsjFunc(cTituloModal,cMensaje,"ACEPTAR",fFuncion);
		}
	}
}

function capturaFirmaUnificada(opc) {
	/*** opc (1-Trabajador/2-Promotor) ***/
	cMensaje = "capturaFirmaUnificada => " + opc;
	grabarRegistro(cMensaje);
	if(opc == 1){opcionWebService(FIRMA_TRABAJADOR);}
	else{opcionWebService(FIRMA_PROMOTOR);}
}

function respuestaFirmaUnificada(resp){
	var fFuncion 	= "";
	var mensajePersona = "";
	if(iOpcion == FIRMA_TRABAJADOR){
		fFuncion = "capturaFirmaUnificada(1);"
		mensajePersona = " del TRABAJADOR"
	}else if (iOpcion == FIRMA_PROMOTOR){
		fFuncion = "capturaFirmaUnificada(2);"
		mensajePersona = " del PROMOTOR";
	}
	switch (resp) {
		case 1:
			//Exito
			if(iOpcion == FIRMA_TRABAJADOR){
				if(iHuella == 0){
					//mdlMsjFunc(cTituloModal,"Firma de Agente Promotor.","ACEPTAR","capturaFirmaUnificada(2);");
					capturaFirmaUnificada(2);
				}
				else{
					obtenerLigasImpresiones(0);
				}
			}else if (iOpcion == FIRMA_PROMOTOR){
				obtenerLigasImpresiones(0);
			}
		break;
		case 2:
			cMensaje = "PROMOTOR: Error desconocido. Favor de capturar nuevamente la firma" +mensajePersona+".";
			mdlMsjFunc(cTituloModal,cMensaje,"ACEPTAR",fFuncion);
		break;	
		case -1:
		case 0:
			if(OSName == "Android"){
				
				if(iHuella == 0){
					cMensaje = "PROMOTOR: La captura de firma "+mensajePersona+" fue cancelada. Favor de intentar de nuevo.";
					mdlMsjFunc(cTituloModal,cMensaje,"ACEPTAR",fFuncion);
				}
				else{
					cMensaje = "PROMOTOR: se canceló la recaptura de la firma "+mensajePersona+".";
					mdlMsjFunc(cTituloModal,cMensaje,"ACEPTAR","mdlDesbloquearMsj()");
				}

			}else{
				if(iHuella == 0){
					cMensaje = "Promotor: deseas salir de la firma.";
					console.log(cMensaje);
					cierraModal();
				}
				else{
					cMensaje = "PROMOTOR: se cancelará la recaptura de la firma.";
					console.log(cMensaje);
					mdlDesbloquearMsj();
				}
			}
		break;
		case -6:
			cMensaje = "PROMOTOR: Favor de conectar la singpad.";
			mdlMsjFunc(cTituloModal,cMensaje,"ACEPTAR",fFuncion);
		break;
		default:
			cMensaje = "PROMOTOR: Error desconocido. Favor de intentar de nuevo.";
			mdlMsjFunc(cTituloModal,cMensaje,"ACEPTAR","cierraModal();");
		break;
	}
}

function respuestaDigitalizador(resp) {
	var bResp = true;
	llamadaCat();
	switch (resp) {
		case 1:
			bResp = false;
			if(OSName == "Android"){
				publicarImagenesMovil();
			}
		break;
		case -1:
			cMensaje = "PROMOTOR: Ocurrio un error al digitalizar la documentación. Favor de volverlo a intentar.";
		break;
		case 2:
			cMensaje = "PROMOTOR: Favor de digitalizar nuevamente los documentos.";
		break;
		case 0:
			cMensaje = "PROMOTOR: La digitalización fue cancelada.";
		break;
		default:
			cMensaje = "PROMOTOR: Error desconocido. Favor de intentar de nuevo.";
		break;
	}
	if(bResp){
		$("#btnDigitaliza").attr("disabled",false);
		mdlMsj(cTituloModal,cMensaje);
	}
}

function obtenerLigasImpresiones(opc){
	var promesas 	= [];
	arrImpresiones 	= [];
	var urlPdf 		= "";
	arrDatosEnvio 	= { iFolioSol: iFolioSol, iFolioEnrol: arrDatosAfi.folioenrol, iEmpleado: iEmpleado, iTipoSol: arrDatosAfi.tiposolicitud, iEnrol: enrol, cModulo: arrIps.ipmodulo };
	$.post(ligaCase, { opcion: 3, funcion: "obtenerligasimpafi", arrdatos: arrDatosEnvio }
	).done(function (result) {
		if (result.estatus == 1) {
			$.each(result.registros, async function(key, value){
				if(value.idliga != 14){//El id 14 es para la creacion de los PDF juntos
					//Arreglo con la detonacion de todas las impresiones
					promesas.push(detonarImpresiones(value,key));
				}else{
					urlPdf = value.destino;
				}
			});
			//Detonar todas las impresiones al mismo tiempo y realizar accion hasta que terminen todas
			$.when.apply($, promesas).then(() =>{
				detonarPdf(urlPdf,opc);
			});
		} else {
			cMensaje = "Error - API obtener ligas impresiones -> " + result.descripcion;
			grabarRegistro(cMensaje);
			mdlMsjFunc(cTituloModal,cMensaje,"ACEPTAR","cierraModal()");
		}
	});
}

function detonarImpresiones(value,i){
	return $.get(value.destino, {rutapdf: arrImpresiones}
	).done(function (result) {
		arrImpresiones.push({"respuesta": result.respuesta, "rutapdf": result.rutapdf, orden: i, "mensaje": result.mensaje, "id": value.idliga});
	}).fail(function (xhr) {
		cMensaje = xhr.responseText;
		grabarRegistro(cMensaje);
		mdlMsjFunc(cTituloModal,cMensaje,"ACEPTAR","cierraModal()");
	});
}

function detonarPdf(ruta,opc){
	var bImp = true;
	//Ordernar PDF
	arrImpresiones.sort(function (a, b){
		if (a.orden > b.orden) {return 1;}
		if (a.orden < b.orden) {return -1;}
		return 0;
	});
	//Validar que no se tenga algun error con una impresion
	$.each(arrImpresiones, function(key, value){
		//Respuesta 0 es un error en las impresiones, el ID 13 es de la credencial del promotor el cual puede no generarse
		if(value.respuesta == 0 && value.id != 13){
			cMensaje = value.mensaje;
			bImp = false;
		}
	});
	console.log(arrImpresiones);
	if(bImp){
		$.post(ruta, {folio: iFolioSol, rutapdf: arrImpresiones, imp: opc}
		).done(function (result) {
			console.log(result);
			if(result.respuesta == 1){
				rutaRelativa 	= result.ruta;
				nombrePdfUFAF 	= result.nombre;
				mostrarDocumentos(opc,result.rutapdf);
			}else{
				cMensaje = "PROMOTOR: Se presento un problema al crear los documentos, favor de contactar a mesa de ayuda.";
				grabarRegistro(cMensaje);
				mdlMsjFunc(cTituloModal,cMensaje,"ACEPTAR","cierraModal()");
			}
		});
	}else{
		mdlMsjFunc(cTituloModal,cMensaje,"ACEPTAR","cierraModal()");
	}
}

function mostrarDocumentos(opc,ruta) {
	rutaUFAF 	= ruta.replace("/sysx/progs/web/","../");
	var url 	= rutaDoc + "file=" + rutaUFAF + "&imprimir=" + opc;
	$('#ifDocs').prop("src", url);
	if(opc == 0){
		$("#btnFirmaTrab").attr("disabled",false);
		$("#btnFirmaProm").attr("disabled",false);
		$("#btnExpediente").attr("disabled",false);
	}else{
		$("#btnDigitaliza").attr("disabled",false);
	}
	setTimeout(() => {
		mdlDesbloquearMsj();
	}, 1200);
}

function llamadaCat(){
	arrDatosEnvio = { iFolioSol: iFolioSol, iTipoSol: arrDatosAfi.tiposolicitud };
	$.post(ligaCase, { opcion: 3, funcion: "guardarllamadacatafiliacion", arrdatos: arrDatosEnvio }
	).done(function (result) {
		console.log(result);
		if (result.estatus == 1) {
			if (result.registros.respuesta == 1 || result.registros.respuesta == 2) {
				if(OSName== "Android"){
					//mdlMsjFunc(cTituloModal,"PROMOTOR: La digitalización se realizo con exito.","ACEPTAR","Android.volverMenu('1');");
					Android.volverMenu('1');
				}else{
					//mdlMsjFunc(cTituloModal,"PROMOTOR: La digitalización se realizo con exito.","ACEPTAR","window.top.closeNavegador();");
					window.top.closeNavegador();
				}

			}else{
				mdlMsjFunc(cTituloModal,"Promotor: Ocurrió un problema al guardar en base de datos, si el problema persiste favor de contactar a mesa de ayuda.","ACEPTAR","cierraModal()");
			}
		} else {
			cMensaje = "Error - API Detonar llamada CAT -> " + result.descripcion;
			grabarRegistro(cMensaje);
			mdlMsjFunc(cTituloModal,cMensaje,"ACEPTAR","cierraModal()");
		}
	}).fail(function (xhr) {
		cMensaje = xhr.responseText;
		grabarRegistro(cMensaje);
		mdlMsjFunc(cTituloModal,cMensaje,"ACEPTAR","cierraModal()");
	});
}
//#endregion

function medioEntrega(){

	$(".btn-coppel").attr("disabled",true);
		cMensaje =
			"<p>Promotor: Favor de seleccionar un medio de entrega para el Expediente de Afiliación</p>" +
			"<select name='slcEntrega' id='slcEntrega' class='custom-select'>" +
			"	<option value='0'>SELECCIONE...</option>" +
			"	<option value='1'>IMPRIMIR</option>" +
			"	<option value='2'>CORREO ELECTRÓNICO</option>" +
			"</select>";

	mdlMsjFunc(cTituloModal,cMensaje,"","");
	$("#myModal .modal-footer").css("display", "none");
	$("#slcEntrega").focus();
}

function imprimirDocumnetos(){
	mdlEspere(cTituloModal);
	entregaExpediente(1,2,"",0);
	if (arrDatosAfi.tiposolicitud == 27){
		//Volvera generar el DRN vertical para una corracta impresion
		obtenerLigasImpresiones(2);
	}else{
		mostrarDocumentos(2,rutaUFAF);
	}
}

function capturaCorreo(){
	cMensaje =
	'<div class="row form-group">' +
	'	<div class="col-md-2"><label>Correo:</label></div>' +
	'	<div class="col-md-5"><input name="ipCorreo01" id="ipCorreo01" type="text" class="form-control form-control-sm expCorreo validarDatosEntrada" maxlength="40"/></div>' +
	'	<div class="col-md-1 text-center"><label>@</label></div>' +
	'	<div class="col-md-4"><select name="slcCorreo01" id="slcCorreo01" class="custom-select"></select></div>' +
	'	<div class="none confirmacion01 col-md-2"><label>Dominio:</label></div>' +
	'	<div class="none confirmacion01 col-md-5"><input name="ipDominio01" id="ipDominio01" type="text" class="form-control form-control-sm expCorreo validarDatosEntrada" maxlength="40"/></div>' +
	'</div>' +
	'<div class="row form-group">' +
	'	<div class="col-md-2"><label>Confirmación:</label></div>' +
	'	<div class="col-md-5"><input name="ipCorreo02" id="ipCorreo02" type="text" class="form-control form-control-sm correo02 expCorreo validarDatosEntrada" maxlength="40"/></div>' +
	'	<div class="col-md-1 text-center"><label>@</label></div>' +
	'	<div class="col-md-4"><select name="slcCorreo02" id="slcCorreo02" class="custom-select"></select></div>' +
	'	<div class="none confirmacion02 col-md-2"><label>Dominio:</label></div>' +
	'	<div class="none confirmacion02 col-md-5"><input name="ipDominio02" id="ipDominio02" type="text" class="form-control form-control-sm correo02 expCorreo validarDatosEntrada" maxlength="40"/></div>' +
	'	<div class="padding-top none error01 col-md-12 text-center"><p class="font-italic text-danger"></p></div>' +
	'</div>';
	var fFuncion1 = "medioEntrega();";
	var fFuncion2 = "validarCorreo();";
	mdlMsjFuncPreg(cTituloModal,cMensaje,"Sin correo",fFuncion1,"Enviar Expediente",fFuncion2);
	$("#myModal .modal-dialog").addClass("modal-xl");
	comboCorreo();
}

function comboCorreo(){
	//Extraer correo y dominio
	var email 		= arrDatosAfi.email.split('@').shift().toUpperCase().trim();
	var dominio 	= arrDatosAfi.email.split('@').pop().toUpperCase().trim();
	var iSelected 	= 7;
	//Iniciar el combo del correo
	$("#slcCorreo01").append($("<option>", {value: "", text: "SELECCIONE UN DOMINIO..."}));
	$("#slcCorreo02").append($("<option>", {value: "", text: "SELECCIONE UN DOMINIO..."}));
	//Llenar combo del correo
	$.each(arrCatCorreo, function(key, value){
		$("#slcCorreo01").append($("<option>", {value: value.idcorreo, text: value.dscorreo.toUpperCase()}));
		$("#slcCorreo02").append($("<option>", {value: value.idcorreo, text: value.dscorreo.toUpperCase()}));
		//Validar el dominio obtenido
		if(dominio.toUpperCase() == value.dscorreo.toUpperCase()){iSelected = value.idcorreo;}
	});
	if(arrDatosAfi.email.trim() != ''){
		$("#ipCorreo01").val(email);
		$("#slcCorreo01").val(iSelected);
		if(iSelected == 7){
			$(".confirmacion01").removeClass("none");
			$("#ipDominio01").val(dominio);
		}
	}
	$("#ipCorreo01").focus();
}

function validarCorreo(){
	$("#myModal .modal-footer button").attr("disabled",true);
	var iRerror 		= true;
	var correo01 		= $("#ipCorreo01").val().trim();
	var correo02 		= $("#ipCorreo02").val().trim();
	var dominio01 		= $("#slcCorreo01 option:selected").val();
	var dominio02 		= $("#slcCorreo02 option:selected").val();
	var dominioText01	= $("#slcCorreo01 option:selected").text();
	var dominioOtro01 	= '';
	var dominioOtro02 	= '';
	var iDatos 			= false;
	var correo			= "";

	if(correo01 != "" && correo02 != "" && dominio01 != "" && dominio02 != ""){
		iDatos = true;
		if(dominio01 == 7 || dominio02 == 7 ){
			dominioOtro01 	= $("#ipDominio01").val().trim();
			dominioOtro02 	= $("#ipDominio02").val().trim();

			if(dominioOtro01 == "" || dominioOtro02 == ""){
				iDatos = false;
			}
		}
	}

	if(iDatos){
		if((correo01 == correo02) && (dominio01 == dominio02) && (dominioOtro01 == dominioOtro02)){
			if(dominio01 != 7){
				correo = correo01 + "@" + dominioText01;
			}else{
				correo = correo01 + "@" + dominioOtro01;
			}

			console.log(correo);

			if(validarEstructuraCorreo(correo)){
				$(".error01").addClass("none");
				entregaExpediente(1,1,correo,1);
				iRerror = false;
			}else{
				$(".error01").removeClass("none");
				$(".error01 p").text("PROMOTOR: correo no valido.");
			}

		}else{
			$(".error01").removeClass("none");
			$(".error01 p").text("PROMOTOR: correo electrónico no coincide, favor de revisar los campos.");
		}
	}else{
		$(".error01").removeClass("none");
		$(".error01 p").text("PROMOTOR: favor de capturar todos los datos para el correo.");
	}

	if(iRerror){
		$("#myModal .modal-footer button").attr("disabled",false);
	}

}

function validarEstructuraCorreo(correo){
	if (/^(([^<>()[\]\.,;:\s@\"]+(\.[^<>()[\]\.,;:\s@\"]+)*)|(\".+\"))@(([^<>()[\]\.,;:\s@\"]+\.)+[^<>()[\]\.,;:\s@\"]{2,})$/i.test(correo)){
		return  true;
	}else{
		return false;
	}
}

function entregaExpediente(os,entrega,correo,estado){
	cMensaje = "os-> [" + os + "] entrega-> [" + entrega + "] correo-> [" + correo + "] estado-> [" + estado + "]";
	grabarRegistro(cMensaje);
	//opcion (1-insertar/2-actualizar), folio, 'correo', entrega (1-imp/2-cor), os (1-modulo/2-movil), estado(0), 'ruta', 'nombre'
	arrDatosEnvio = {iOpcion: 1, iFolioSol: iFolioSol, sCorreo: correo, iEntrega: entrega, iOs: os, iEstado: estado, sRuta: rutaRelativa, sNombre: nombrePdfUFAF};
	$.post(ligaCase,{ opcion: 3, funcion: "entregaexpedienteafiliacion", arrdatos: arrDatosEnvio }
	).done(function (result) {
		if (result.estatus == 1) {
			if (result.registros.respuesta == 1) {
				if(entrega==1){
					envioCorreo();
				}
			}else{
				mdlMsjFunc(cTituloModal,"Promotor: Ocurrió un problema con el medio de entrega, si el problema persiste favor de contactar a mesa de ayuda.","ACEPTAR","cierraModal()");
			}
		} else {
			cMensaje = "Error - API validar medio de entrega -> " + result.descripcion;
			grabarRegistro(cMensaje);
			mdlMsjFunc(cTituloModal,cMensaje,"ACEPTAR","cierraModal()");
		}
	}).fail(function (xhr) {
		cMensaje = xhr.responseText;
		grabarRegistro(cMensaje);
		mdlMsjFunc(cTituloModal,cMensaje,"ACEPTAR","cierraModal()");
	});
}

function envioCorreo(){

	arrDatosEnvio = {iFolioSol: iFolioSol, sIpSpa: arrIps.ipspa};
	$.post(ligaCase,{ opcion: 3, funcion: "enviarcorreo", arrdatos: arrDatosEnvio }
	).done(function (result) {
		if (result.estatus == 1) {
			cMensaje = "Correo enviado con exito folio-> " + iFolioSol;
			grabarRegistro(cMensaje);
		}
		if(OSName == "Android"){
			mdlEspere(cTituloModal);
			setTimeout(function() {
				mdlDesbloquearMsj();
				$("#btnDigitaliza").attr("disabled",false);
			}, 7000);
			
		}else{
			mdlDesbloquearMsj();
			$("#btnDigitaliza").attr("disabled",false);
		}
		
	}).fail(function (xhr) {
		cMensaje = xhr.responseText;
		grabarRegistro(cMensaje);
		mdlMsjFunc(cTituloModal,cMensaje,"ACEPTAR","cierraModal()");
	});
}

/**************************************************************************************************************************************
 * <SERVICIO WEBX>
**************************************************************************************************************************************/
//#region
function opcionWebService(opcion) {
	mdlEspere(cTituloModal);
	iOpcion 	= opcion;
	var ruta 	= "";
	var param 	= "";
	if(OSName == "Android"){
		switch (iOpcion) {
			case HUELLA_TRABAJADOR:
				Android.llamaComponenteHuellas(7, iFolioSol, 1, 0, iEmpleado, 0, arrDatosAfi.curp);
			break;
			case HUELLA_PROMOTOR:
				Android.llamaComponenteHuellas(4, iEmpleado, 1, 1, iEmpleado, 0, arrDatosAfi.curp);
			break;
			case FIRMA_TRABAJADOR:
				Android.llamaComponenteFirmaD(iFolioSol, 0, 1);
			break;
			case FIRMA_PROMOTOR:
				Android.llamaComponenteFirmaD(iFolioSol, 0, 2);
			break;
			case DIGITALIZADOR:
				Android.llamaComponenteDigitalizador(iFolioSol, iEmpleado, 0, 1, tipodigitalizacion);
			break;
		}
	}else{
		switch (iOpcion) {
			case HUELLA_TRABAJADOR:
				ruta 	= "C:\\SYS\\PROGS\\HETRABAJADOR.EXE";
				param 	= "3 2 "+ arrDatosAfi.curp + " " + iEmpleado + " " + iFolioSol + " " + iBusqueda + " " + arrDatosAfi.tiposolicitud;
			break;
			case HUELLA_PROMOTOR:
				ruta 	= "C:\\SYS\\PROGS\\HETRABAJADOR.EXE";
				param 	= "2 2 "+ arrDatosAfi.curp + " " + iEmpleado + " " + iFolioSol + " " + "2 " + " " + arrDatosAfi.tiposolicitud;
			break;
			case FIRMA_TRABAJADOR:
				MensajePar = '"Firma de Trabajador para Expediente de Afiliación"';
				ruta 	= "C:\\SYS\\SIGNPAD\\CAPTURAFIRMAAFORE.EXE";
				param 	= "1 1 UFAF_" + iFolioSol + "_FTRAB"+" 1 "+MensajePar;
			break;
			case FIRMA_PROMOTOR:
				MensajePar = '"Firma de Promotor para Expediente de Afiliación"';
				ruta 	= "C:\\SYS\\SIGNPAD\\CAPTURAFIRMAAFORE.EXE";
				param 	= "1 1 UFAF_" + iFolioSol + "_FPROM"+" 1 "+MensajePar;
			break;
			case DIGITALIZADOR:
				ruta 	= "C:\\SYS\\PAFSCANIMGNET\\DIGIDOCTOSCLIENTE.EXE";
				param 	= "1 1 " + iFolioSol + " " + iEmpleado;
			break;
		}
		ejecutaWebService(ruta,param);
	}
}

function ejecutaWebService(sRuta, sParametros){

	if(OSName != "Android"){

		console.log("iOpcion [" + iOpcion + "] sRuta [" + sRuta + "] sParametros [" + sParametros + "]");
		soapData 		= "", httpObject = null, docXml = null, iEstado = 0, sMensaje = "";
		var sUrlSoap 	= "http://127.0.0.1:20044/";
		var bResp		= false;
		soapData 		=
		'<?xml version=\"1.0\" encoding=\"UTF-8\"?>' +
		'<SOAP-ENV:Envelope' +
		' 	xmlns:SOAP-ENV=\"http://schemas.xmlsoap.org/soap/envelope/\"'+
		' 	xmlns:SOAP-ENC=\"http://schemas.xmlsoap.org/soap/encoding/\"'+
		' 	xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\"'+
		' 	xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\"'+
		' 	xmlns:ns2=\"urn:ServiciosWebx\">'+
		'	<SOAP-ENV:Body  SOAP-ENV:encodingStyle=\"http://schemas.xmlsoap.org/soap/encoding/\">'+
		'		<ns2:ejecutarAplicacion>'+
		'			<inParam>'+
		'				<Esperar>1</Esperar>'+
		'				<RutaAplicacion>' + sRuta + '</RutaAplicacion>'+
		'				<parametros><![CDATA[' + sParametros + ']]></parametros>'+
		'			</inParam>'+
		'		</ns2:ejecutarAplicacion>'+
		'	</SOAP-ENV:Body>'+
		'</SOAP-ENV:Envelope>';
		httpObject = getHTTPObject();
		if(httpObject){
			if(httpObject.overrideMimeType){httpObject.overrideMimeType("false");}
			httpObject.open('POST', sUrlSoap, true); //-- no asincrono
			httpObject.setRequestHeader("Accept-Language", null);
			httpObject.onreadystatechange = function(){
				if(httpObject.readyState == 4 && httpObject.status == 200){
					parser 	= new DOMParser();
					docXml 	= parser.parseFromString(httpObject.responseText, "text/xml");
					iEstado = docXml.getElementsByTagName('Estado')[0].childNodes[0].nodeValue;
					setTimeout(function() {
						respuestaWebService(iEstado);
					}, 100);
					bResp = true;
				}
				return bResp;
			};
			httpObject.send(soapData);

		}

		function getHTTPObject(){
			var xhr = false;
			if (window.ActiveXObject) {
				try {xhr = new ActiveXObject("Msxml2.XMLHTTP");}
				catch(e) {
					try {xhr = new ActiveXObject("Microsoft.XMLHTTP");}
					catch(e) {xhr = false;}
				}
			} else if (window.XMLHttpRequest) {
				try {xhr = new XMLHttpRequest();}
				catch(e) {xhr = false;}
			}
			return xhr;
		}
	}
}

function respuestaWebService(iRespuesta) {
	var resp = parseInt(iRespuesta);
	console.log("iOpcion [" + iOpcion + "] iRespuesta [" + resp + "]");
	grabarRegistro(" iOpcion en respuestaWebService() ");
	switch (iOpcion) {
		case HUELLA_TRABAJADOR:
		case HUELLA_PROMOTOR:
			respuestaHuellaSegundoSello(resp);
		break;
		case FIRMA_TRABAJADOR:
		case FIRMA_PROMOTOR:
			respuestaFirmaUnificada(resp);
		break;
		case DIGITALIZADOR:
			respuestaDigitalizador(resp);
		break;
	}
}

function cierraModal()
{
	if(OSName == "Android"){
		Android.volverMenu('1');
	}else{
		window.top.cerrarIframe();
	}
}
//#endregion

/**************************************************************************************************************************************
 * <FUNCIONES APP MOVIL>
**************************************************************************************************************************************/
//#region
//Respuesta de firma en Android
function getComponenteFirma(statusFirma, opcionFirma){
	statusFirma = parseInt(statusFirma);

	if(statusFirma == 1){
		if(iOpcion == FIRMA_TRABAJADOR){
			nombreHuella = "1 1 UFAF_" + iFolioSol + "_FTRAB.JPG";
			if(OSName =="Android"){
				var newName = "UFAF_" + iFolioSol + "_FTRAB.JPG";
				cambiarNombreImg(iFolioSol, newName);
			}
		}else if(iOpcion == FIRMA_PROMOTOR){
			nombreHuella = "1 1 UFAF_" + iFolioSol + "_FPROM.JPG";
			if(OSName =="Android"){
				var newName = "UFAF_" + iFolioSol + "_FPROM.JPG";
				cambiarNombreImg(iFolioSol, newName);
			}
		}
	}else{
		respuestaWebService(0);
	}
}

//Respuesta del digitalizador en Android
function getComponenteDigitalizar(statusDig){
	respuestaDigitalizador(parseInt(statusDig));
}
//Cambiar el nombre de la firma capturada en Android
function cambiarNombreImg(iFolioSol, newName){

	var oldName = "FARE_"+ iFolioSol +"_IMPRE.JPG";

	arrDatosEnvio = {oldName:oldName, newName: newName};
	$.post(ligaCase,{ opcion: 4, funcion: "renombrarImgFirmas", arrdatos: arrDatosEnvio }
	).done(function (data) {
		if(parseInt(data) == 1){
			respuestaFirmaUnificada(1);
		}else{
			respuestaFirmaUnificada(2);
		}
		console.log(data);
	});
}
//Publicacion de imagenes en Android
function publicarImagenesMovil(){
	arrDatosEnvio = {folio:iFolioSol};
	$.post(ligaCase,{ opcion: 4, funcion: "subirImgServidorIntermedio", arrdatos: arrDatosEnvio }
	).done(function (data) {
		console.log(data);
	});
}

/* Funcion que recibe la respuesta del componente EnrolMovil */
function getComponenteHuella(estatusProceso, jsonFingers, opcionHuella, jsonTemplate) {

	var iTipoServicio = 9910;
	var sTipoOperacion = '';

	if (arrDatosAfi.tiposolicitud == 'Registro' || arrDatosAfi.tiposolicitud == '26' || arrDatosAfi.tiposolicitud == '33') {
		sTipoOperacion = '0405';
	}
	else {
		sTipoOperacion = '0404';
	}

	if (parseInt(opcionHuella) == 0) {
		// HUELLA TRABAJADOR
		mdlEspere(cTituloModal);
		grabarRegistro("jsonFinger: " + jsonFingers);
		document.getElementById("fingerString0").value = jsonFingers;
		var template = JSON.parse(jsonTemplate);
		document.getElementById("fingerTemplate0").value = template["FingerTemplate"][0]["Template"];

		validarHuellas(opcionHuella, 3, iTipoServicio, sTipoOperacion);

	}else{
		// HUELLA PROMOTOR
		mdlEspere(cTituloModal);
		document.getElementById("fingerString1").value = jsonFingers;
		var template = JSON.parse(jsonTemplate);
		document.getElementById("fingerTemplate1").value = template["FingerTemplate"][0]["Template"];

		validarHuellas(opcionHuella, 2,iTipoServicio, sTipoOperacion );
	}
}

function validarHuellas(oHuella, iTipoP, iTipoServicio, sTipoOperacion) { //14

	var bRetorna = '';

	if (oHuella == 0) {
		var cTemplate = $('#fingerTemplate0').val();
	}
	if (oHuella == 1) {
		var cTemplate = $('#fingerTemplate1').val();
	}

		arrDatosEnvio = {iNumFuncionario: iEmpleado, iFolioSolicitud: iFolioSol, iTipoPerson: iTipoP, cTemplate: cTemplate};
		$.post(ligaCase,{ opcion: 4, funcion: "validarHuellaCertificado", arrdatos: arrDatosEnvio }
		).done(function (data) {
			if (parseInt(data) == 1) {
				enrolhuellasservicio(oHuella, iTipoP, iTipoServicio, sTipoOperacion);
			}else{
				respuestaWebService(101);
			}
		});
}

function enrolhuellasservicio(oHuella, iTipoP, iTipoS, iTipoO) { //17

	var bRetorna = '';
	if (oHuella == 0) {
		// HUELLA TRABAJADOR
		var base64Imagen = $('#fingerString0').val();
		var cTemplate = $('#fingerTemplate0').val();
	}
	if (oHuella == 1) {
		// HUELLA PROMOTOR
		var base64Imagen = $('#fingerString1').val();
		var cTemplate = $('#fingerTemplate1').val();
	}

	var base64Imagen = JSON.parse(base64Imagen);
	var iNist = base64Imagen["Fingerprints"][0]["Nfiq"];
	var sDispositivo = base64Imagen["Serial"];
	var sImagen = base64Imagen["Fingerprints"][0]["B64FingerImage"];
	var sTipoServicio = "";


		arrDatosEnvio = {
			sCURPTrab: arrDatosAfi.curp,
			iNumFuncionario: iEmpleado,
			iFolioSolicitud: iFolioSol,
			iTipoPerson: iTipoP,
			iTipoServ: iTipoS,
			sTipoOperac: iTipoO,
			iNumDedo: 2,
			iNist: iNist,
			sDispositivo: sDispositivo,
			iFap: 50,
			sImagen: sImagen,
			sTipoServicio: sTipoServicio,
			cTemplate: cTemplate
		};

		$.post(ligaCase,{ opcion: 4, funcion: "validarHuella", arrdatos: arrDatosEnvio }
		).done(function (data) {
			bRetorna = data;
			mdlDesbloquearMsj();
			respuestaWebService(parseInt(data));
		});

	return bRetorna;
}
//#endregion

function mostrarMensajeCierreSesion()
{
    $(".content-loader").hide();
    $("#divMensaje").dialog({
        title: "",
        autoOpen: true,
        resizable: false,
        width: "350",
        height: "auto",
        modal: true,
        closeOnEscape: false, //Deshabilitar el boton escape para eviar que se cierre el dialogo
        position: { my: "center", at: "center", of: $("body"), within: $("body") }, //Posicionar el dialog en el centro
        open: function (event, ui) {
            $(".ui-dialog-titlebar-close", ui.dialog).hide();
            $(this).html("<p> PROMOTOR: La sesión expiró. Favor de iniciar sesión nuevamente </p>");
        },
        buttons: {
            Aceptar: function () {
                $(this).dialog("close");
                cerrarSesion();
            }
        }
    });
}

function cerrarSesion() {
    if (OSName == 'Android') {
        Android.cerrarSesion();
    } else {
       ligaMenuAfore();
    }
}

function ligaMenuAfore() {

	$.post(ligaCase,{ opcion: 4, funcion: "ligaMenuAfore", arrdatos: arrDatosEnvio}
	).done(function (data) {
		bRetorna = data.respuesta;
        arrDatosEnvio = {codigoEmpleado: iEmpleado};
        $.post(ligaCase,{ opcion: 5, funcion: "eliminarToken", arrdatos: arrDatosEnvio }
		).done(function (data) {
			location.href = bRetorna;
		});
	});
}

function consultarrespuestawebservices() {/////////////// INCIDENCIA 432442 
	
	var datos = '';
	arrDatosEnvio = { iFolioSol: iFolioSol, iEmpleado: iEmpleado };
	$.post(ligaCase, { opcion: 3, funcion: "consultarrespuestawebservices", arrdatos: arrDatosEnvio }
	).done(function (result) {
		if (result.estatus == 1) {
			datos = result;
			
			selloverificacionbiom = datos.registros.selloverificacion;
			diagnosticovoluntram  = datos.registros.diagnostico;
			mensaje2do	      	  = datos.registros.mensaje;
			resultadoverificacion = datos.registros.resultadoverificacion;
						
		} else {
			cMensaje = "Error - API información 2do Sello -> " + result.descripcion;
			grabarRegistro(cMensaje);
			mdlMsjFunc(cTituloModal,cMensaje,"ACEPTAR","cierraModal()");
		}
	});
}