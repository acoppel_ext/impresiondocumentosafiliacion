<?php
	include_once("../../distboostrap4/php/definiciones.php");
	$arrResp 		= array();
	$iOpcion 		= isset($_POST['opcion']) 	? $_POST['opcion'] 		: 0;
	$sFuncion 		= isset($_POST['funcion']) 	? $_POST['funcion'] 	: '';
	$arrDatos 		= isset($_POST['arrdatos']) ? $_POST['arrdatos'] 	: '';
	$codigoEmpleado = isset($_POST['codigoEmpleado']) ? $_POST['codigoEmpleado'] 	: '';

	class CGeneral extends CMetodoGral{
		public static $cNombreLog = "UnificacionFirmas";

		public static function grabarRegistro($texto){
			CMetodoGral::setLogName(CGeneral::$cNombreLog);
			CMetodoGral::grabarLogx("[" . __METHOD__ . "]" . $texto);
			$arrDatos = array("texto" => $texto);
			return $arrDatos;
		}

		public static function datosPagina(){
			$arrDatos = array("fechaactual"=> "", "ipspa" => "", "ipmodulo" => "");
			setlocale(LC_TIME, "es_ES");
			$arrBD 						= CMetodoGral::getDatosBD("AFOREGLOBAL");
			$arrDatos["fechaactual"] 	= utf8_encode(strftime("%A, %d de %B de %Y"));
			$arrDatos["ipspa"]			= $arrBD["AFOREGLOBAL"]["BDIP"];
			$arrDatos["ipmodulo"]		= CMetodoGral::getRealIP();
			CMetodoGral::setLogName(CGeneral::$cNombreLog);
			return $arrDatos;
		}

		public static function consumirApi($sClase, $sFuncion, $arrDatos){
			//IP Server
			$host = $_SERVER["HTTP_HOST"];
			$xml = simplexml_load_file("../../conf/apiconfig.xml");
			$ipServidor = "";
			foreach($xml->item as $elemento)
			{
				if( $host == $elemento->id){
					$ipServidor = $elemento->value;
				}
			}
			if($ipServidor == ""){
				$ipServidor = $xml->default;
			}

			$url = 'http://'.$ipServidor.'/apirestafiliacion/codeigniter/api/'.$sClase.'/'.$sFuncion.'/';
			
			//API key
			$apiKey 	= '1234';
			//Auth credentials
			$username 	= "admin";
			$password 	= "1234";
			//create a new cURL resource
			$ch 		= curl_init($url);
			
			//Solicitar Token 
			$requiereToken = false;
			$componenteActual = "impresiondocumentosafiliacion";
			foreach($xml->componentes as $componente)
			{
				if( $componenteActual  == $componente->id){
					$requiereToken = $componente->value;
				}
			}

			$arrDatos["componente"] = $requiereToken === 'true'? true: false;
			
			curl_setopt($ch, CURLOPT_TIMEOUT, 30);
			curl_setopt($ch, CURLOPT_RETURNTRANSFER,1);
			curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_ANY);
			if(isset($_SESSION["authorization"])){
				curl_setopt($ch, CURLOPT_HTTPHEADER, array("X-API-KEY: " . $apiKey, "Authorization: " . $_SESSION["authorization"]));
			}else{
				curl_setopt($ch, CURLOPT_HTTPHEADER, array("X-API-KEY: " . $apiKey));
			}
			curl_setopt($ch, CURLOPT_USERPWD, "$username:$password");
			curl_setopt($ch, CURLOPT_POST, 1);
			curl_setopt($ch, CURLOPT_POSTFIELDS, $arrDatos);
			$result = curl_exec($ch);
			//close cURL resource
			curl_close($ch);

			if($result)
				$result = json_decode($result,true);
			return $result;
		}
	}

	switch ($iOpcion) {
		case 1: $arrResp = CGeneral::grabarRegistro($arrDatos); break;
		case 2: $arrResp = CGeneral::datosPagina(); break;
		case 3: $arrResp = CGeneral::consumirApi("CtrlimpresionDocumentosafiliacion", $sFuncion, $arrDatos); break;
		case 4: $arrResp = CGeneral::consumirApi("CtrlconstanciaAfiliacion", $sFuncion, $arrDatos); break;
		case 5: $arrResp = CGeneral::consumirApi("CtrlAuth", $sFuncion, $arrDatos); break;
	}
	echo json_encode($arrResp);
?>